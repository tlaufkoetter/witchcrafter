import random
import time
import os
MAX_CARDS = 60
MAX_VALUE_P = 13
MAX_VALUE_W_J = 4

TYPES = {'':'', 
         'Jester':'Jester',
         'Wizard':'Wizard'}

COLORS = {'red':'red',
		  'blue':'blue', 
		  'green':'green', 
		  'yellow':'yellow',
		  'white':'white'}

class Event(object):
    pass

class Observable(object):
    def __init__(self):
        self.callbacks = []
    def subscribe(self, callback):
        self.callbacks.append(callback)
    def fire(self, **attrs):
        e = Event()
        e.source = self
        for k, v in attrs.iteritems():
            setattr(e, k, v)
        for fn in self.callbacks:
            fn(e)


class Card(object):
	"""container class for cards"""
	def __init__(self, value, color, type):
		self.value = value
		self.color = color
		self.type = type
		self.highest = False
		self.trump = False

class Player(Observable):
	"""container class for players"""
	def __init__(self, name, type, dealer):
		super(Player, self).__init__()
		self.name = name
		self.score = 0
		self.hand = []
		self.type = type
		self.dealer = dealer
		self.turn_not_over = False
		self.turning = False
		self.guess_value = 0

	def __bot_guess__(self, current_round):
		"""the bot will randomly guess his tricks"""
		random_guess = random.randint(0,current_round.counter)
		self.guess_value = random_guess
		time.sleep(1)

	def guess(self, current_round):
		"""lets the player make his guess"""
		if self.type == "NPC":
			self.__bot_guess__(current_round)
		else:
			self.turn_not_over = True
			self.fire(source="guess", pl=self, current_round=current_round)		
			while self.turn_not_over:
				pass
		return self.guess_value

	def __chose_card__(self):
		"""if there is a room for choice, a random card will be picked"""
		if len(self.hand) > 0:
			random_position = random.randint(0,len(self.hand)-1)
			card_position = random_position
		return self.hand[card_position]

	def __determine_illegal_colors__(self, current_round):
		"""determines which colors are illegal"""
		illegal_colors = []
		for c,p in current_round.stack:
			is_wizard = c.type == TYPES['Wizard']
			is_jester = c.type == TYPES['Jester']
			if is_wizard:
				break
			elif not is_jester:
				for card in self.hand:
					if card.color == c.color:
						illegal_colors = [color for color in COLORS if not color == 'white']
						illegal_colors.remove(card.color)
						break
				break

		return illegal_colors

	def __bot_turn__(self, cr, illegal_colors):
		"""the bot will randomly play a legal card"""
		chosen_card_is_illegal = True
		while chosen_card_is_illegal:
			
			card_from_hand = self.__chose_card__()
			chosen_card_is_illegal = card_from_hand.color in illegal_colors
			if not chosen_card_is_illegal:
				self.hand.remove(card_from_hand)
				break
		time.sleep(1)
		return card_from_hand

	def make_turn(self, current_round):
		"""lets the player make his turn"""
		illegal_colors = self.__determine_illegal_colors__(current_round)
		if self.type == "NPC":
			bot_card = self.__bot_turn__(current_round, illegal_colors)
			return bot_card
		else:
			self.turn_not_over = True
			self.fire(source="make_turn", pl=self, current_round=current_round, illegal_colors=illegal_colors)		
			while self.turn_not_over:
				pass
			return None

	


class Round(Observable):
	"""container for the current round"""
	def __init__(self, counter, players, deck):
		super(Round, self).__init__()
		self.counter = counter
		self.deck = deck
		self.stack = []
		self.players = players
		self.scores = {}
		self.guesses = {}

		for p in self.players:
			self.scores[p] = 0
			self.guesses[p] = 0

		self.number_of_players = len(players)
		self.round_cards = self.__generate_round_cards__()
		self.__distribute_cards__()

	def __generate_round_cards__(self):
		"""generates the deck used for this round"""
		round_cards = self.deck[:]
		max_hand_cards = MAX_CARDS / self.number_of_players
		number_of_cards = self.number_of_players * self.counter + 1 if self.counter < max_hand_cards else max_hand_cards
		for j in xrange(MAX_CARDS - number_of_cards):
			random_card = random.randint(0, len(round_cards)-1)
			round_cards.pop(random_card)
		return round_cards

	def __distribute_cards__(self):
		"""distributes the cards among the players"""
		for p in self.players:
			p.hand = []
			for i in xrange(self.counter):
				random_position = random.randint(1,len(self.round_cards)-1)
				random_card = self.round_cards.pop(random_position)
				p.hand.append(random_card)

	def __determine_trump_card__(self):
		"""determines the trump card"""
		all_cards_distributed = len(self.round_cards) == 0
		last_card_jester = self.round_cards[0].type == TYPES['Jester']
		if not all_cards_distributed and not last_card_jester:
			self.trump = self.round_cards[0]
			self.round_cards[0].trump = True
		else:
			self.trump = None

	def __make_guesses__(self):
		"""runs through the players to make them guess their tricks"""
		self.fire(source='scores', players=self.players, scores=self.scores, guesses=self.guesses)
		for p in self.players:
			p.turning = True
			self.fire(source='scores', players=self.players, scores=self.scores, guesses=self.guesses)
			self.guesses[p] = p.guess(self)
			self.fire(source='scores', players=self.players, scores=self.scores, guesses=self.guesses)
			p.turning = False
			self.fire(source='scores', players=self.players, scores=self.scores, guesses=self.guesses)

	def __run_queue__(self):
		"""queues all players by who won last round"""
		players = self.players[:]
		for p in players:
			if p.dealer:
				n = players.index(p)
				for i in xrange(n):
					players.append(players.pop(0))
				break
		self.players = players

	def __make_turns__(self):
		"""runs through the players to let them make their turns"""
		self.fire(source='scores', players=self.players, scores=self.scores, guesses=self.guesses)
		for p in self.players:
			p.turning = True
			self.fire(source='scores', players=self.players, scores=self.scores, guesses=self.guesses)
			card = p.make_turn(self)
			bot_made_turn = not card == None 
			if bot_made_turn:
				self.play_card(card, p)
			p.turning = False
			self.fire(source='scores', players=self.players, scores=self.scores, guesses=self.guesses)

	def __determine_turns_winner(self):
		"""determines who played the winning card this turn"""
		for c,p in self.stack:
			card_is_highest = c.highest
			if card_is_highest:
				self.scores[p] += 1
				is_dealer=True
			else:
				is_dealer=False
			i = self.players.index(p)
			self.players[i].dealer = is_dealer

	def __set_scores__(self):
		"""checks the players' guess/trick ratio and sets their scores"""
		for p in self.scores:
			balance = abs(self.scores[p] - self.guesses[p])
			guessed_right = balance == 0
			if guessed_right:
				p.score += 20 + 10 * self.guesses[p]
			else:
				p.score -= 10 * balance

	def run_round(self):
		"""follows through the steps of a round"""
		self.__determine_trump_card__()
		self.fire(source='play', stack=self.stack, trump=self.trump)
		self.__run_queue__()
		self.__make_guesses__()

		round_over = self.players[0].hand == []
		while not round_over:
			self.__run_queue__()
			self.__make_turns__()
			self.fire(source='scores', players=self.players, scores=self.scores, guesses=self.guesses)
			self.__determine_turns_winner()
			round_over = self.players[0].hand == []
			for c,p in self.stack:
				c.highest = False
			self.stack = []
			self.fire(source='scores', players=self.players, scores=self.scores, guesses=self.guesses)

		for c in self.round_cards:
			c.trump = False
		self.__set_scores__()
		self.fire(source='scores', players=self.players, scores=self.scores, guesses=self.guesses)

	def __validate_current_trump__(self, card):
		"""validates current trump"""
		trump_needed = not self.trump == None
		if trump_needed:
			trump_is_wizard = self.trump.type == TYPES['Wizard']
			if trump_is_wizard:
				card_is_jester = card.type == TYPES['Jester']
				if not card_is_jester:
					self.trump.trump = False
					self.trump = card
					self.trump.trump = True
					self.fire(source='play', stack=self.stack, trump=self.trump)

	def __determine_highest_card__(self, card, player):
		"""determines the actual highest card in the current stack"""
		is_first_card = self.stack == []
		if is_first_card:
			card.highest = True
			self.stack.append((card,player))
		else:
			for c,p in self.stack:
				if c.highest:
					cards_have_same_color = c.color == card.color
					played_cards_value_is_higher = c.value < card.value
					stack_card_is_white = c.color == COLORS['white']


					played_card_is_wizard = card.type == TYPES['Wizard']
					stack_card_is_wizard = c.type == TYPES['Wizard'] 
					stack_card_is_jester = c.type == TYPES['Jester'] 
					played_card_is_jester = card.type == TYPES['Jester'] 

					played_card_is_trump = card.color == self.trump.color
					stack_card_is_trump = c.color == self.trump.color

					if (cards_have_same_color and played_cards_value_is_higher and not stack_card_is_wizard) \
					or (played_card_is_wizard and not stack_card_is_wizard) \
					or (played_card_is_trump and not stack_card_is_trump and not stack_card_is_wizard) \
					or (stack_card_is_jester and not played_card_is_jester):
						card.highest = True
						self.stack.append((card,player))
						i = self.stack.index((c,p))
						c.highest = False
						self.stack[i] = (c,p)
					else:
						self.stack.append((card,player))
					break

	def play_card(self, card, player):
		"""puts played card on top of the stack"""
		self.__validate_current_trump__(card)
		self.__determine_highest_card__(card, player)
		self.fire(source='play', stack=self.stack, trump=self.trump)
		
		

class Session(Observable):
	"""container class for the current game session"""
	def __init__(self,  player_number, human_name):
		super(Session, self).__init__()
		self.current_round = None
		self.players = self.__generate_player_list__(player_number, human_name)
		self.roundcounter = 0
		self.deck = self.__generate_deck__()
		self.max_rounds = MAX_CARDS / player_number

	def __generate_player_list__(self, player_number, human_name):
		"""generates player list"""
		players = [Player(human_name, "Human", True)]
		players.extend([Player('Bot'+str(i), "NPC", False) for i in xrange(player_number-1)])
		return players

	def __generate_deck__(self):
		"""generates the wizard deck of cards"""
		deck = 	[Card(value, color, TYPES['']) for value in xrange(MAX_VALUE_P) for color in COLORS if not color == 'white']
		deck.extend([Card(value, COLORS['white'], type) for value in xrange(MAX_VALUE_W_J) for type in [TYPES['Wizard'],TYPES['Jester']]])
		return deck

	def __is_game_over__(self):
		"""checks whether the game is over, 
		if yes, the application exits for now"""
		game_over = not self.roundcounter+1 < self.max_rounds
		if game_over:
			os._exit(0)

	def next_round(self):
		"""generates the next round"""
		self.__is_game_over__()

		self.roundcounter += 1
		self.current_round = Round(self.roundcounter, self.players, self.deck)
		self.fire(source='subscribe', current_round=self.current_round)
		self.current_round.run_round()
		self.fire(source='endround', current_round=self.current_round, session=self)