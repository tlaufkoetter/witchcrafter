import Tkinter as tk
import tkMessageBox as tkm
from models import Session
from thread import start_new_thread

class MainView(tk.Tk):
	"""class for the game view stuff"""
	def __init__(self):
		tk.Tk.__init__(self)
		self.title("Witchcrafter")
		self.geometry('700x250')
		
		StartView(self)
		self.mainloop()

class StartView(tk.Frame):
	"""the view for the start menu"""
	def __init__(self, main_window):
		tk.Frame.__init__(self, master=main_window)
		self.main_window = main_window

		tk.Label(self, text="Name").grid(row=0)
		tk.Label(self, text='Number of NPCs').grid(row=1)

		self.ename = tk.Entry(self)
		self.snpcs = tk.Scale(self, from_=2, to=5, orient=tk.HORIZONTAL)
		self.ename.grid(row=0, column=1)
		self.snpcs.grid(row=1, column=1)

		bstart = tk.Button(self, text='Start', width=25, command=self.__start__)
		bquit = tk.Button(self, text='Quit', width=25, command=quit)
		bstart.grid(row=2)
		bquit.grid(row=2, column=1)

		self.pack()

	def __start__(self):
		name=self.ename.get()
		npcs=self.snpcs.get()
		if name=='':
			tkm.showerror('User Name', 'Username must not be empty')
		else:
			session = Session(npcs+1,name)
			player = session.players[0]
			self.destroy()
			GameView(self.main_window, session, player)

class GameView(tk.Frame):
	"""the view for the game"""
	def __init__(self, main_window, session, player):
		tk.Frame.__init__(self, master=main_window)
		self.main_window = main_window

		session.subscribe(self.update_round)
		session.subscribe(self.update_subscribe)
		player.subscribe(self.update_hand)
		player.subscribe(self.update_guess)
		

		self.hand_frame = tk.Frame(self)
		self.stack_frame = tk.Frame(self)
		self.scores_frame = tk.Frame(self)

		self.cards_in_frames = {self.hand_frame:[], self.stack_frame:[]}
		self.slabels = []


		self.cv = tk.IntVar()

		self.bend_turn = tk.Button(self, text='End Turn', width=25, command=lambda player=player:self.__end_turn__(player), state='disabled')
		self.bend_turn.grid(column=2, row=0)

		self.bnext_round = tk.Button(self, text='Next Round', width=25, command=lambda session=session:self.__next_round__(session))
		self.bnext_round.grid(column=3, row=0)

		self.stricks = tk.Scale(self, from_=0, to=1, state='disabled')
		self.stricks.grid(column=1, row=1, sticky='nes')

		self.bguess = tk.Button(self, text='Guess', width=25, command=lambda player=player:self.__guess__(player), state='disabled')
		self.bguess.grid(column=0, row=0, columnspan=2)

		self.pack()

	def update_guess(self, e):
		if not e.source=="guess":
			return
		
		pl = e.pl
		hand = pl.hand
		cr = e.current_round
		
		self.__insert_cards__(hand, self.hand_frame, (1,2), 'stack')
		self.stricks.config(to=cr.counter, state='normal')
		self.bguess.config(state='normal')	

	def update_round(self, e):
		"""enables the next round button, when a round is over"""
		if not e.source=='endround':
			return
		self.stricks.config(to=e.current_round.counter)
		self.bnext_round.config(state='normal')

	def update_subscribe(self, e):
		"""subscribes update_stack and update_scores to the current round"""
		if not e.source=='subscribe':
			return
		current_round = e.current_round
		current_round.subscribe(self.update_stack)
		current_round.subscribe(self.update_scores)


	def update_stack(self, e):
		"""updates the stack view"""
		if not e.source == 'play':
			return
		stack = e.stack
		trump = e.trump
		if not trump == None:
			cards = [trump]
		for c,p in stack:
			c.type = c.type
			cards.append(c)

		self.__insert_cards__(cards, self.stack_frame, (1,3), 'stack')

	def update_scores(self, e):
		if not e.source == 'scores':
			return
		players = e.players
		scores = e.scores
		guesses = e.guesses
		for l in self.slabels:
			l.destroy()
		self.slabels = []
		for p in players:
			turning = p.turning
			txt = p.name, p.score, '('+str(scores[p])+'/'+str(guesses[p])+')'
			l = tk.Label(self.scores_frame, text=txt)
			if turning:
				l.config(font='bold')
			l.pack()
			self.slabels.append(l)
		self.scores_frame.grid(row=1, column=0, sticky='wens')

	def __insert_cards__(self, cards, frame, position, card_type, illegal_colors=[]):
		"""inserts cards cards into the given frame"""
		# frame.grid_forget()
		# frame = tk.Frame(self)
		
		for c in self.cards_in_frames[frame]:
			c.destroy()
		self.cards_in_frames[frame] = []
		self.cv = tk.IntVar()
		for i in xrange(len(cards)):
			card = cards[i]
			value = str(card.value+1)
			color = card.color
			type = card.type
			highest = card.highest
			trump = card.trump

			txt = ('Trump:' if trump else '')+type+ (value if type=='' else '') + ('*' if highest else '')

			if card_type == 'radio':
				r = tk.Radiobutton(frame, 
		                text=txt,
		                width=25,
		                variable=self.cv,
		                value=i,
		                anchor='nw',
		                bg=color)
				if card.color in illegal_colors:
					r.config(state='disabled')
					if i == self.cv.get():
						self.cv.set(self.cv.get()+1)
			elif card_type == 'stack':
				r = tk.Label(frame,
						text=txt,
						width=25,
						anchor='nw',
						bg=color)
			r.pack(anchor='n')
			self.cards_in_frames[frame].append(r)
		row,column = position
		frame.grid(row=row, column=column, sticky='nw')

	def __fn__(self, pl, current_round):
		card = pl.hand.pop(self.cv.get())
		current_round.play_card(card,pl)
		self.bend_turn.config(state='disabled')
		self.__insert_cards__(pl.hand, self.hand_frame, (1,2), 'stack')
		pl.turn_not_over = False

	def update_hand(self, e):
		if not e.source == "make_turn":
			return
		pl = e.pl
		current_round = e.current_round
		illegal_colors = e.illegal_colors

		hand = pl.hand
		self.__insert_cards__(hand, self.hand_frame, (1,2), 'radio', illegal_colors=illegal_colors)
		
		self.bend_turn.config(state='normal', command=lambda pl=pl, current_round=current_round:self.__fn__(pl, current_round))

	def __next_round__(self, session):
		self.bnext_round.config(state='disabled')
		start_new_thread(session.next_round, ())

	def __end_turn__(self):
		self.bend_turn.config(state='disabled')

	def __guess__(self, pl):
		pl.guess_value = self.stricks.get()
		self.bguess.config(state='disabled')
		self.stricks.config(state='disabled')
		pl.turn_not_over = False